<?hh
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\fileSystem\driver\memory
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;
	use nuclio\plugin\fileSystem\driver\memory\MemoryException;
	use nuclio\plugin\config\ConfigCollection;
	use \Memcached;
	
	/**
	 * Manage file system functionalities on server's memory.
	 *
	 * This class provides the ability to manage CURD over files hosted on the server memory
	 * 
	 */
	<<provides('fileSystem::memory')>>
	class Memory extends Plugin implements CommonInterface
	{
		private string $basePath = 'fileSystem_memory/';
		private Memcached $cache;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Memory
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct():void
		{
			parent::__construct();
			$this->cache = new Memcached();
			$this->cache->addServer('localhost', 11211);
			$this->cache->setOption(Memcached::OPT_COMPRESSION, false); //this is necessary to make append/prepend possible
		}

		/**
		 * Check if the file exist or not.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return bool       	True/False for file existance.
		 */
		public function exists(string $path):bool
		{
			// (fopen($this->basePath.$path, "r") || opendir($this->basePath.$path))?return true: return false;
			return ($this->isFile($path) || $this->isDirectory($path));
		}

		/**
		 * Check if the path given is a file.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile(string $path):bool
		{
			// fopen($this->basePath.$path, "r")?return true: return false;
			$this->cache->get($this->basePath.$path)?$return = true: $return = false;
			return $return;
		}

		/**
		 * Get content of a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function read(string $path):string
		{
			// opendir($this->basePath.$path)?return true: return false;
			if($this->isFile($path))
			{
				return $this->cache->get($this->basePath.$path);	
			}	
			throw new MemoryException("Not a file");
			
		}

		/**
		 * Overwrite file or create if does not exist.
		 * 
		 * @access public
		 * 
		 * @param  string $path     Path to the file.
		 * @param  string $contents Content to be added to the file.
		 * @return bool          	True/False for the write process.
		 */
		public function write(string $path, string $contents):bool
		{
			return $this->cache->set($this->basePath.$path, $contents);
		}

		/**
		 * Append content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path 	Path to the file.
		 * @param  string $contents Content to be append
		 * @return bool   	 		True/False for the append process.
		 */
		public function append(string $path, string $contents):bool
		{
			// $handle = fopen($this->basePath.$path, 'a');
			// fwrite($handle, $contents)?return true: return false;
			return $this->cache->append($this->basePath.$path, $contents);
		}

		/**
		 * Creates an empty file at hte passed path
		 * 
		 * @access public
		 * 
		 * @param  string $path the path to the file to be created
		 * @return bool       	true/false on success/failure
		 */
		public function touch(string $path):bool
		{
			// $handle = fopen($this->basePath.$path, 'a');
			// fwrite($handle,'')?return true: return false;
			return $this->cache->append($this->basePath.$path, '');
		}

		/**
		 * Delete file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file to be deleted.
		 * @return mixed       	True if delete success, error message if failed.
		 */
		public function delete(string $path, int $rules):bool
		{
			return $this->cache->delete($this->basePath.$path);	
		}

		/**
		 * Move file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be moved.
		 * @param  string $target 	Where to move.
		 * @return bool         	True/False for the move process.
		 */
		public function move(string $path, string $target, int $rules):bool
		{
			if($this->isFile($path))
			{
				$result = $this->cache->set($this->basePath.$target, $this->read($path));
				if($result)
				{
					$result = $this->cache->delete($this->basePath.$path);
				}
				return $result;
			}
			return false;
		}

		/**
		 * Copy file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be copied.
		 * @param  string $target 	Where to copied.
		 * @return bool         	True/False for the copied process.
		 */
		 
		public function copy(string $path, string $target):bool
		{
			if($this->isFile($path))
			{
				return $this->cache->set($this->basePath.$target, $this->read($path));
			}
			return false;
		}

		/**
		 * Get the file name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName(string $path):?string
		{
			if($this->isFile($path))
			{
				$name = end(explode('/', $path));
				return reset(explode('.', $name));	
			}
			return null;
		}

		/**
		 * Get the file extension.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt(string $path):?string
		{
			if($this->isFile($path))
			{
				$name = end(explode('/', $path));
				return end(explode('.', $name));	
			}
			return null;	
		}

		/**
		 * Get the file type.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType(string $path):?string
		{
			if($this->isFile($path))
			{
				$name = end(explode('/', $path));
				return end(explode('.', $name));	
			}
			return null;
		}

		/**
		 * Get the file size.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize(string $path):int
		{
			if($this->isFile($path))
			{
				return strlen($this->read($path));
			}
			return 0;
		}

		/**
		 * Check whether the param given is path or not.
		 * 
		 * @access public
		 * 
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory(string $directory):bool
		{
			$list = $this->listAllFilesAndFolders($directory);
			$list->count()>0 ? $return = true : $return = false;
			return $return;
		}

		/**
		 * Determine if empty.
		 *
		 * @param      string   $directory  Path of directory
		 *
		 * @return     boolean  True if empty, False otherwise.
		 */
		public function isEmpty(string $directory):bool
		{
			$list = $this->listAllFilesAndFolders($directory);
			$list->count()==0 ? $return = true:$return = false;
			return $return;
		}

		/**
		 *
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable(string $path):bool
		{
			return true;
		}

		/**
		 * List all the files in the direotory.
		 * 
		 * @access public
		 * 
		 * @param  string $directory 	Path of directory
		 * @return ImmVector  			List of files.
		 */
		public function listAllFilesAndFolders(string $directory):Vector<string>
		{
			$keys = $this->cache->getAllKeys();
			$result = Vector{};
			if(is_null($keys))
			{
				return $result;
			}
			foreach ($keys as $key) 
			{
				//check if $key starts with $directory
				if($directory === '' || strrpos($key, $this->basePath.$directory, -strlen($key)) !== false)
				{
					$name = str_replace($this->basePath.$directory, '', $key);
					if($name=='')
					{
						continue;
					}
					$result->add($name);
				}
			}
			return $result;
		}

		/**
		 * create directory
		 * 
		 * @access public
		 * 
		 * @param  string       $path      	Where to create directory.
		 * @param  int  		$mode      	File mode. Eg. 0777 for read/write/execute
		 * @param  bool 		$recursive 	Allows the creation of nested directories specified in the path
		 * @return bool                 	True/False for file created or not.
		 */
		public function create(string $path, int $mode = 0755, bool $recursive = false):bool
		{
			return $this->cache->set($this->basePath.$path, '');
		}
		
		
		/**
		 * Determine if readable.
		 * 
		 * @access public
		 *
		 * @param      string   $path   file or directory path
		 *
		 * @return     boolean  True if readable, False otherwise.
		 */
		public function isReadable(string $path):bool
		{
			return true;
		}
		
		/**
		 * Look for file or directory
		 * 
		 * @access public
		 * 
		 * @param  string $path  Path to perform the search into.
		 * @param  string $regex regex expression to be matched
		 * @return string List of all the search results
		 */
		public function find(string $path, string $regex):Vector<string>
		{
			$keys = $this->cache->getAllKeys();
			$result = Vector{};
			foreach ($keys as $key) 
			{
				if($path === '' || strrpos($key, $this->basePath.$path, -strlen($key)) !== false)
				{
					$name = str_replace($this->basePath.$path, '', $key);
					if($name=='')
					{
						continue;
					}
					else if(preg_match($regex, $name))
					{
						$result->add($key);
					}
				}
			}
			return $result;
		}

		/**
		 * Delete all files inside directory.
		 * 
		 * @access public
		 * 
		 * @param  string $directory 	Path to directories
		 * @return bool            		True/False for successfully deleted.
		 */
		public function deleteFilesInDir(string $directory):bool
		{
			$keys = $this->cache->getAllKeys();
			$return = true;
			foreach ($keys as $key) 
			{
				//check if $key starts with $directory
				if($directory === '' || strrpos($key, $this->basePath.$directory, -strlen($key)) !== false)
				{
					if($key==$this->basePath.$directory)
					{
						continue;
					}
					$result = $this->cache->delete($key);
					if(!$result)
					{
						$return = false;
					}
				}
			}
			return $return;
		}
	}
}	
